# README #

### C++ implementation of Kalman filter ###

* source: http://greg.czerniak.info/guides/kalman1/

### Accessing contents of this repository ###

* the best way to access this code is to use graphicall git client like [SourceTree](https://www.sourcetreeapp.com/),

* another option is to use command line [git-scm](https://git-scm.com/downloads),

* or You could just download zip with code from this repository - **not recommended, because changes are not tracked like in two options above**.


### Things left to do ###

* implement motor action according to r(t),

* tune PID controller (too slow, less I, more P and D),

* tune Kalman filter (it is slow, Tp should not be ignored),

* Matrix.inverse() is implemented for 1x1 matricies only, it might be needed to implement inverse of matrix for size 2x2 or larger,

* divide code to \*.ino and \*.h files for convenience.


### Basics ###

Here are the most important concepts you need to know:

* Kalman Filters are discrete. That is, they rely on measurement samples taken between repeated but constant periods of time. Although you can approximate it fairly well, you don't know what happens between the samples.

* Kalman Filters are recursive. This means its prediction of the future relies on the state of the present (position, velocity, acceleration, etc) as well as a guess about what any controllable parts tried to do to affect the situation (such as a rudder or steering differential).

* Kalman Filters work by making a prediction of the future, getting a measurement from reality, comparing the two, moderating this difference, and adjusting its estimate with this moderated value.

* The more you understand the mathematical model of your situation, the more accurate the Kalman filter's results will be.

* If your model is completely consistent with what's actually happening, the Kalman filter's estimate will eventually converge with what's actually happening.

When you start up a Kalman Filter, these are the things it expects:

* The mathematical model of the system, represented by matrices A, B, and H.
* An initial estimate about the complete state of the system, given as a vector x.
* An initial estimate about the error of the system, given as a matrix P.
* Estimates about the general process and measurement error of the system, represented by matrices Q and R.

During each time step, you are expected to give it the following information:

* A vector containing the most current control state (vector "u"). This is the system's guess as to what it did to affect the situation (such as steering commands).
* A vector containing the most current measurements that can be used to calculate the state (vector "z").

After the calculations, you get the following information:

* The most current estimate of the true state of the system.
* The most current estimate of the overall error of the system.