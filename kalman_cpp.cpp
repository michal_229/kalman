#include <iostream>
#include <stdio.h>      /* printf */
#include <math.h>       /* sin */
#include <string>

// matrix class
class Matrix {
public:
    double** mat;
    int rowNum;
    int colNum;

    // constructor with 3 params
    Matrix(int nRows, int nCols, double fill) {
        rowNum = nRows;
        colNum = nCols;

        mat = new double*[rowNum];

        for(int r = 0; r < rowNum; r++) {
            mat[r] = new double[colNum];
            for(int c = 0; c < colNum; c++) {
                mat[r][c] = fill;
            }
        }
    }

    // constructor with 2 params redirected to const. with 3 params, one specified
    Matrix(int nRows, int nCols) : Matrix(nRows, nCols, 0.0) {}

    // empty constructor
    Matrix() {}

    // destructor // FIXME: memory deallocation does not work properly, so commented - need to fix
    // ~Matrix() {
    //     for(int r = 0; r < rowNum; r++) {
    //         delete[] mat[r];
    //     }
    //
    //     delete[] mat;
    // }

    // print this matrix
    void print() { // TODO: make it printf, not cout
        for (int r = 0; r < rowNum; r++) {
            for (int c = 0; c < colNum; c++) {
                //printf("%f\n", mat[r][c]);
                std::cout << mat[r][c] << "\t";
            }
            std::cout << "\n";
        }
        std::cout << "\n";
    }


    // multiply this matrix by matrix B
    Matrix multiply(Matrix matB) { // C = A*B
        Matrix matC(rowNum, matB.colNum);

        for (int r = 0; r < rowNum; r++) {
            for (int c = 0; c < matB.colNum; c++) {
                for (int i = 0; i < colNum; i++) {
                    matC.mat[r][c] += mat[r][i]*matB.mat[i][c];
                }
            }
        }

        return matC;
    }

    // multiply this matrix by double/integer
    // template<typename T> // not needed anymore
    Matrix multiply(double k) {
        Matrix matC(rowNum, colNum);

        for (int r = 0; r < rowNum; r++) {
            for (int c = 0; c < colNum; c++) {
                matC.mat[r][c] = mat[r][c] * k;
            }
        }

        return matC;
    }

    // transpose this matrix
    Matrix transpose() {
        Matrix matC(colNum, rowNum);

        for (int r = 0; r < rowNum; r++) {
            for (int c = 0; c < colNum; c++) {
                matC.mat[c][r] = mat[r][c];
            }
        }

        return matC;
    }

    // add
    Matrix add(Matrix matB) {
        Matrix matC(rowNum, colNum);

        for (int r = 0; r < rowNum; r++) {
            for (int c = 0; c < colNum; c++) {
                matC.mat[c][r] = mat[c][r] + matB.mat[c][r];
            }
        }

        return matC;
    }

    // subtract matrix B from this matrix
    Matrix subtract(Matrix matB) {
        Matrix matC(rowNum, colNum);

        for (int r = 0; r < rowNum; r++) {
            for (int c = 0; c < colNum; c++) {
                matC.mat[c][r] = mat[c][r] - matB.mat[c][r];
            }
        }

        return matC;
    }

    // inverse of this matrix
    Matrix inverse() { // this handles 1x1 matricies only
        Matrix matC(rowNum, colNum, 0);

        if ( rowNum == 1 && colNum == 1 )
            matC.mat[0][0] = 1/mat[0][0]; // simple inv for 1x1 matrix
        else if ( (rowNum == 2) && (colNum = 2) )
            printf("Matrix.inv() not implemented for given matrix size: 2x2"); // TODO: implement for size 2x2
        else
            printf("Matrix.inv() not implemented for given matrix size: %ix%i", rowNum, colNum); // TODO: implement for size nxn, n>2

        return matC;
    }
};


class KalmanFilterLinear {
public:
    Matrix A;
    Matrix B;
    Matrix H;
    Matrix current_state_estimate;
    Matrix current_prob_estimate;
    Matrix Q;
    Matrix R;

    KalmanFilterLinear(Matrix _A, Matrix _B, Matrix _H, Matrix _x, Matrix _P, Matrix _Q, Matrix _R) {
        A = _A;
        B = _B;
        H = _H;
        current_state_estimate = _x;
        current_prob_estimate = _P;
        Q = _Q;
        R = _R;
    }

    double getCurrentState() { // TODO: this could return Matrix object
        return current_state_estimate.mat[0][0];
    }

    void step(Matrix control_vector, Matrix measurement_vector) { // TODO: review and check if equations are correct
        Matrix predicted_state_estimate = (A.multiply(current_state_estimate)).add(B.multiply(control_vector));
        Matrix predicted_prob_estimate = ((A.multiply(current_prob_estimate)).multiply(A.transpose())).add(Q);

        //     #--------------------------Observation step-----------------------------
        Matrix innovation = measurement_vector.subtract(H.multiply(predicted_state_estimate));
        Matrix innovation_covariance = (H.multiply(predicted_prob_estimate).multiply(H.transpose())).add(R);

        //     #-----------------------------Update step-------------------------------
        Matrix kalman_gain = predicted_prob_estimate.multiply(H.transpose()).multiply(innovation_covariance.inverse());
        current_state_estimate = predicted_state_estimate.add(kalman_gain.multiply(innovation));
        //     # We need the size of the matrix so we can make an identity matrix.
        int size = current_prob_estimate.rowNum;
        //     # eye(n) = nxn identity matrix.,
        Matrix eye(size, size, 0); for (int i = 0; i < size; i++) eye.mat[i][i] = 1;
        current_prob_estimate = (eye.subtract(kalman_gain.multiply(H))).multiply(predicted_prob_estimate);
    }
};


class Controller {
public:
    double Kp;
    double Ki;
    double Kd;
    double Ts;
    double e_old;

    double pVal;
    double iVal;
    double dVal;

    double pidOutVal;

    Controller(double _kp, double _ki, double _kd, double _ts) {
        Kp = _kp;
        Ki = _ki;
        Kd = _kd;
        Ts = _ts;

        e_old = 0;

        pVal = 0;
        iVal = 0;
        dVal = 0;

        pidOutVal = 0;
    }

    void step(double e) { // e - error; (u(t) - kalman(t)) -> e(t) -> [PID] -> r(t) -> [DC motor] -> y(t) -> [kalman] -> kalman(t)
        // computing and updating state, returning control signal value to controlled system

        pVal = e*Kp;
        iVal += e*Ki*Ts;
        dVal = (e - e_old)*Kd/Ts;
    }

    double getControlSignalValue() { // optional
        // return current control signal value to controlled system

        pidOutVal = pVal + iVal + dVal;

        return pidOutVal;
    }

};


class Voltmeter {
public:
    double amplitude;
    double noiselevel;

    Voltmeter(double amp, double noislvl) { // constructor
        amplitude = amp;
        noiselevel = noislvl;
    }

    double getVoltage(int step) {
        return amplitude*sin(((double) step) * 3.1415/180.0 );
    }

    double getVoltageWithNoise(int step) {
        return getVoltage(step) + getRandom(step) - noiselevel/2;
    }

    double getRandom(int i) { // this returns numbers evenly distributed in range <0, noiselevel>; TODO: make normal distributed random numbers
        return fmod(i * 3213.1544789, noiselevel);
    }
};


void test() { // TODO: tests for Matrix methods behavior
    Voltmeter voltmeter(1.0, 0.25);

    for (int i = 0; i < 600; i++)
        printf("%i : %f : %f\n", i, voltmeter.getVoltageWithNoise(i), voltmeter.getRandom(i));

    Matrix a(4, 3, 1); // 4 rows, 3 cols, fill with 1
    Matrix b(3, 4, 2); // 3 rows, 4 cols, fill with 2

    Matrix c = b.multiply(a);

    c.multiply(2).print();

    a.print();

    a.transpose().print();
}



void compute_kalman() {
    // variable to trick kalman to have less phase difference from measured signal
    int divider = 10;

    // number of samples to compute
    int numsteps = 600 * divider;

    // creating initial matricies
    Matrix A(1, 1, 1);
    Matrix H(1, 1, 1);
    Matrix B(1, 1, 0);
    Matrix Q(1, 1, 0.00001);
    Matrix R(1, 1, 0.1);
    Matrix xhat(1, 1, 3);
    Matrix P(1, 1, 1);

    // creating kalman filter and voltimeter objects to use
    KalmanFilterLinear filter(A, B, H, xhat, P, Q, R);
    Voltmeter voltmeter(1.0, 0.25);

    // arrays for computed values
    double measuredvoltage[numsteps];
    double truevoltage[numsteps];
    double kalman[numsteps];

    // computing real voltage, voltage with noise and kalman estimation
    for (int i = 0; i < numsteps; i++) {
        double measured = voltmeter.getVoltageWithNoise(i/divider);
        measuredvoltage[i] = measured;
        truevoltage[i] = voltmeter.getVoltage(i/divider);
        kalman[i] = filter.getCurrentState();
        Matrix controlVec(1, 1, 0);
        Matrix measuredValVec(1, 1, measured);
        filter.step(controlVec, measuredValVec);
    }

    // printing computed values from arrays
    for (int i = 0; i < numsteps; i++) {
        printf("%f : %f : %f\n", truevoltage[i], measuredvoltage[i], kalman[i]);

    }
}


double u;
double e;
double r;
double y;
double y_kal;

// creating initial matricies
Matrix A(1, 1, 1);
Matrix H(1, 1, 1);
Matrix B(1, 1, 0);
Matrix Q(1, 1, 0.00001);
Matrix R(1, 1, 0.1);
Matrix xhat(1, 1, 3);
Matrix P(1, 1, 1);
Matrix controlVec(1, 1, 0);
Matrix measuredValVec(1, 1, 0);

// creating kalman kalman and voltimeter objects to use
KalmanFilterLinear kalman(A, B, H, xhat, P, Q, R);
Voltmeter voltmeter(1.0, 0.25);
Controller pid(1, 1, 1, 1);


void setup() {
    // compute_kalman();

    u = 5;

    e = 0;
    r = 0;
    y = 0;
    y_kal = 0;

    printf(" u\te\tr\ty\ty_kal \n");
    printf(" %.2f\t%.2f\t%.2f\t%.2f\t%.2f \n", u, e, r, y, y_kal);
}

void loop() {


    y_kal = kalman.getCurrentState();

    y = voltmeter.getVoltageWithNoise(0);
    measuredValVec.mat[0][0] = y;
    kalman.step(controlVec, measuredValVec);

    r = pid.getControlSignalValue();

    e = u - y_kal;
    pid.step(e);

    printf(" %.1f\t%.1f\t%.1f\t%.1f\t%.1f \n", u, e, r, y, y_kal);
}


int main() {
    setup();

    while(1) {
        loop();
    }

    return 0;
}
